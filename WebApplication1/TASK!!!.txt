Implement the application (web-service) to obtain information about the traffic situation (traffic jams) by region.

The application infrastructure includes the following components:
• Webapi with the ability to:
Request available regions;
Region codes {regionCode}, which have a traffic service https://goo.gl/EKCY6i
◦ request traffic data for all regions;
◦ request traffic data for the selected region;
• As a provider for traffic data, use:
◦ yandex-service, which does not always return a successful result;
GET http://hidedoor.com/servlet/redirect.
Get
https://export.yandex.com/bar/reginfo.xml?region={regionCode}&bustCache={timeStamp}
◦ (i) a service stub that can return data (random) only for a specific region;
◦ The choice of current provider can be specified in the configuration file.
• Storage for storing data on traffic jams. You can choose the storage to your taste (file system, Excel, rdbms, No SQL, etc).

When requesting traffic data via webapi, the application must:
• request all available regions;
• request traffic data from the provider for each region;
• save the received data to the storage;
• on subsequent requests to webapi, the data is returned from the repository, if the relevance of the data is less than 1 minute, otherwise request data from the provider.