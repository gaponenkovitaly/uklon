﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Infrastructure.Models;
using WebApplication1.Infrastructure.Services.TrafficJamProviders;
using WebApplication1.Services.RoadService;

namespace WebApplication1.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class RoadStateController : ControllerBase
    {
        private readonly IRegionService regionService;

        private readonly ITrafficJamProvider trafficJamProvider;

        public RoadStateController(
            IRegionService regionService,
            ITrafficJamProvider trafficJamProvider)
        {
            this.regionService = regionService;
            this.trafficJamProvider = trafficJamProvider;
        }

        [HttpGet("regions")]
        public IEnumerable<string> GetAllAvailableRegions()
        {
            return regionService.GetAllAvailableRegions();
        }

        [HttpGet("traffic_jams")]
        public async Task<IEnumerable<string>> GetTrafficJamsByAllRegions()
        {
            var regions = regionService.GetAllAvailableRegions();
            var states = await trafficJamProvider.GetAllRegionTrafficJamState(regions);
            if (states != null && states.Any())
            {
                return states.Select(s => s.State);
            }
            return new[] { "No states loaded" };
        }

        [HttpGet("traffic_jams_by_region")]
        public async Task<string> GetTrafficJamsByRegion(string region)
        {
            if (regionService.IsRegionExist(region))
            {
                var state = await trafficJamProvider.GetTrafficJamStateByRegion(region);
                if (state != null)
                {
                    return state.State;
                }
                return string.Format("Region {0} not loaded", region);
            }
            return string.Format("Region {0} not found", region);
        }
    }
}