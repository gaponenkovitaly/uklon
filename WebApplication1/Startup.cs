﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApplication1.Infrastructure.Repositories;
using WebApplication1.Infrastructure.Repositories.RoadRepository;
using WebApplication1.Infrastructure.Services.TimerUpdate;
using WebApplication1.Infrastructure.Services.TrafficJamProviders;
using WebApplication1.Services.RoadService;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SqlDbContext>(options=>
                options.UseSqlServer(Configuration["Data:ConnectionString"]));
            services.AddTransient<IRoadRepository, RoadRepository>();

            services.AddTransient<IRegionService, RegionService>(
                serviceProvider =>
                {
                    return new RegionService(
                        serviceProvider.GetService<IRoadRepository>(),
                        Configuration,
                        new TimerUpdateRegionService(Configuration));
                });

            services.AddTransient<ITrafficJamProvider, TrafficJamProviderYandex>(
                serviceProvider => new TrafficJamProviderYandex(
                    serviceProvider.GetService<IRoadRepository>(),
                    Configuration,
                    new TimerUpdateTrafficJam(Configuration)));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
