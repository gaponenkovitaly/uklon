﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Infrastructure.Models
{
    [Table("TrafficJamStates")]
    public class RegionTrafficJamState
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Region { get; set; }

        public string TimeStamp { get; set; }

        public string State { get; set; }

        //public RegionTrafficJamState(string region, string timeStamp, string state)
        //{
        //    this.Region = region;
        //    this.TimeStamp = timeStamp;
        //    this.State = state;
        //}
    }
}
