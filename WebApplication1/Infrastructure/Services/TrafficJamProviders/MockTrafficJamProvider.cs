﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using WebApplication1.Infrastructure.Models;

namespace WebApplication1.Infrastructure.Services.TrafficJamProviders
{
    public class MockTrafficJamProvider : ITrafficJamProvider
    {
        private static readonly Random getrandom = new Random();

        private readonly string[] windDirections = { "север", "юг", "восток", "запад" };


        public string GetTrafficJamStateByRegion(string region)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(@"\Infrastructure\Services\TrafficJamProviders\MockTrafficJam.xml");

            ModifyAndRandomXml(xml);
            return xml.InnerXml;
        }

        private void ModifyAndRandomXml(XmlDocument xml)
        {
            var dayPartnodes = xml.SelectNodes("/info/weather/day/day_part");
            foreach (XmlNode node in dayPartnodes)
            {
                node.Attributes["wind_speed"].Value = GetRandomNumber(0, 10).ToString();
                node.Attributes["wind_direction"].Value = windDirections[GetRandomNumber(0, windDirections.Length - 1)];
                /// ...

            }
        }

        public static int GetRandomNumber(int min, int max)
        {
            lock (getrandom)
            {
                return getrandom.Next(min, max);
            }
        }

        public Task<IEnumerable<RegionTrafficJamState>> GetAllRegionTrafficJamState(IEnumerable<string> regions)
        {
            throw new NotImplementedException();
        }

        Task<RegionTrafficJamState> ITrafficJamProvider.GetTrafficJamStateByRegion(string region)
        {
            throw new NotImplementedException();
        }
    }
}
