﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;
using WebApplication1.Infrastructure.Models;
using WebApplication1.Infrastructure.Repositories.RoadRepository;
using WebApplication1.Infrastructure.Services.TimerUpdate;

namespace WebApplication1.Infrastructure.Services.TrafficJamProviders
{
    public class TrafficJamProviderYandex : ITrafficJamProvider
    {
        private readonly ITimerUpdate TimerUpdate;

        private readonly IEnumerable<string> urls;

        private readonly IRoadRepository roadRepository;

        private readonly string ProxyHost;

        private readonly int ProxyPort;

        public TrafficJamProviderYandex(IRoadRepository roadRepository, 
            IConfiguration config,
            ITimerUpdate TimerUpdate)
        {
            urls = config.GetSection("providers:Yandex").Get<List<string>>();
            ProxyHost = config.GetSection("Proxies:TrafficProxy:ProxyHost").Get<string>();
            ProxyPort = config.GetSection("Proxies:TrafficProxy:ProxyPort").Get<int>();
            this.TimerUpdate = TimerUpdate;
            this.roadRepository = roadRepository;
        }

        public async Task<IEnumerable<RegionTrafficJamState>> GetAllRegionTrafficJamState(IEnumerable<string> regions)
        {
            if (TimerUpdate.IsNeedUpdateData())
            {
                var regionTrafficJamState = await LoadRegionTrafficJamStateFromProvider(regions);
                if (regionTrafficJamState.Any())
                {
                    roadRepository.SaveAllTrafficJamState(regionTrafficJamState);
                }
            }
            
            return roadRepository.GetAllTrafficJam();
        }

        public async Task<RegionTrafficJamState> GetTrafficJamStateByRegion(string region)
        {
            if (TimerUpdate.IsNeedUpdateData())
            {
                var regionTrafficJamState = await LoadRegionTrafficJamStateFromProvider(new List<string>() { region });
                if (regionTrafficJamState.Any())
                {
                    roadRepository.SaveTrafficJamState(regionTrafficJamState.First());
                }
                
            }
            return roadRepository.GetTrafficJamByRegion(region);
        }

        private async Task<IEnumerable<RegionTrafficJamState>> LoadRegionTrafficJamStateFromProvider(IEnumerable<string> regions)
        {
            var regionTrafficJamState = new List<RegionTrafficJamState>();
            var timeStamp = DateTimeOffset.Now.ToUnixTimeSeconds().ToString();
            HttpClientHandler aHandler = new HttpClientHandler();
            IWebProxy proxy = new WebProxy(ProxyHost, ProxyPort);
            aHandler.Proxy = proxy;
            HttpClient client = new HttpClient(aHandler);

            foreach (var region in regions)
            {
                var urlsWithParams = urls.Last()
                    .Replace("{regionCode}", region)
                    .Replace("{timeStamp}", timeStamp);
                try
                {
                    var response = await client.GetAsync(urlsWithParams);
                    if (response.IsSuccessStatusCode)
                    {
                        var state = await response.Content.ReadAsStringAsync();
                        regionTrafficJamState.Add(new RegionTrafficJamState() { Region = region, State = state, TimeStamp = timeStamp });
                    }
                }
                catch (HttpRequestException)
                {
                    TimerUpdate.SetTimerDefault();
                }
                
            }

            return regionTrafficJamState;
        }

        
    }
}
