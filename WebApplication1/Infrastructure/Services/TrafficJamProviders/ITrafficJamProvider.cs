﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Infrastructure.Models;

namespace WebApplication1.Infrastructure.Services.TrafficJamProviders
{
    public interface ITrafficJamProvider
    {
        Task<IEnumerable<RegionTrafficJamState>> GetAllRegionTrafficJamState(IEnumerable<string> regions);

        Task<RegionTrafficJamState> GetTrafficJamStateByRegion(string region);
    }
}
