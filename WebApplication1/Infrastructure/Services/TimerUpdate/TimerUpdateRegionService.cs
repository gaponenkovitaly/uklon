﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Infrastructure.Services.TimerUpdate
{
    public class TimerUpdateRegionService : ATimerUpdate
    {
        public static DateTime LastUpdate;

        public TimerUpdateRegionService(IConfiguration config) : base(config) { }

        public override bool IsNeedUpdateData()
        {
            return IsNeedUpdateData(ref LastUpdate);
        }

        public override void SetTimerDefault()
        {
            LastUpdate = DateTime.MinValue;
        }
    }
}
