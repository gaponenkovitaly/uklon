﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Infrastructure.Services.TimerUpdate
{
    public abstract class ATimerUpdate : ITimerUpdate
    {

        public readonly int MinuteExpiresTime;

        public ATimerUpdate(IConfiguration config)
        {
            MinuteExpiresTime = config.GetSection("Global:Services:ExpiresTimeUpdateMinutes").Get<int>();
        }

        public abstract bool IsNeedUpdateData();
        public abstract void SetTimerDefault();

        protected bool IsNeedUpdateData(ref DateTime LastUpdate)
        {
            if (DateTime.UtcNow.Subtract(LastUpdate).TotalMinutes > MinuteExpiresTime)
            {
                LastUpdate = DateTime.UtcNow;
                return true;
            }
            return false;
        }
    }
}
