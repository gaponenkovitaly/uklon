﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Infrastructure.Services.TimerUpdate
{
    public interface ITimerUpdate
    {
        bool IsNeedUpdateData();

        void SetTimerDefault();
    }
}
