﻿using Google.Apis.Auth.OAuth2;
using Google.GData.Client;
using Google.GData.Spreadsheets;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using WebApplication1.Infrastructure.Global;
using WebApplication1.Infrastructure.Models;
using WebApplication1.Infrastructure.Repositories.RoadRepository;
using WebApplication1.Infrastructure.Services.TimerUpdate;

namespace WebApplication1.Services.RoadService
{
    public class RegionService : IRegionService
    {
        private readonly string TrafficRegionData;

        private readonly IRoadRepository roadRepository;

        public readonly ITimerUpdate TimerUpdate;

        public RegionService(IRoadRepository roadRepository, 
            IConfiguration config,
            ITimerUpdate TimerUpdate)
        {
            this.roadRepository = roadRepository;
            TrafficRegionData = config["Traffic:Region:Data"];
            this.TimerUpdate = TimerUpdate;
        }

        public IEnumerable<string> GetAllAvailableRegions()
        {
            SaveAllAvailableRegions();
            return roadRepository.GetAllAvailableRegions().Select(r => r.RegionCode).ToList();
        }

        public bool IsRegionExist(string region)
        {
            SaveAllAvailableRegions();
            return roadRepository.IsRegionExist(region);
        }

        private void SaveAllAvailableRegions()
        {
            if (TimerUpdate.IsNeedUpdateData())
            {
                //const string ServiceAccountEmail = "452351479-q41ce1720qd9l94s8847mhc0toao1fed@developer.gserviceaccount.com";
                //var certificate = new X509Certificate2("Key.p12", "notasecret", X509KeyStorageFlags.Exportable);
                //var serviceAccountCredentialInitializer =
                //    new ServiceAccountCredential.Initializer(ServiceAccountEmail)
                //    {
                //        Scopes = new[] { "https://spreadsheets.google.com/feeds" }
                //    }.FromCertificate(certificate);

                //var credential = new ServiceAccountCredential(serviceAccountCredentialInitializer);

                //if (!credential.RequestAccessTokenAsync(System.Threading.CancellationToken.None).Result)
                //{
                //    throw new InvalidOperationException("Access token request failed.");
                //}
                //var requestFactory = new GDataRequestFactory(null);
                //requestFactory.CustomHeaders.Add("Authorization: Bearer " + credential.Token.AccessToken);
                //var myService = new SpreadsheetsService(null) { RequestFactory = requestFactory };


                //SpreadsheetQuery query = new SpreadsheetQuery();
                //SpreadsheetFeed feed = myService.Query(query);
                //if (feed.Entries.Count > 0)
                //{
                //    AtomLink cellFeedLink = feed.Entries[0].Links.FindService(GDataSpreadsheetsNameTable.CellRel, null);

                //    CellQuery cellQuery = new CellQuery(cellFeedLink.HRef.ToString());
                //    CellFeed cellFeed = myService.Query(cellQuery);

                //    Console.WriteLine("Cells in this worksheet:");
                //    foreach (CellEntry curCell in feed.Entries)
                //    {
                //        Console.WriteLine("Row {0}, column {1}: {2}", curCell.Cell.Row,
                //            curCell.Cell.Column, curCell.Cell.Value);
                //    }
                //}
                roadRepository.SaveAllAvailableRegions(temporaryDataSeed());
            }
        }

        // todo: temporary!!!
        private IEnumerable<Region> temporaryDataSeed()
        {
            return new List<Region>()
            {
                new Region() { RegionCode = "10795", RegionName = "Смоленская область"},
                new Region() { RegionCode = "20523", RegionName = "Электросталь"},
            };
        }
    }
}
