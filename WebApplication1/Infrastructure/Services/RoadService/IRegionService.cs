﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Services.RoadService
{
    public interface IRegionService
    {
        IEnumerable<string> GetAllAvailableRegions();

        bool IsRegionExist(string region);
    }
}
