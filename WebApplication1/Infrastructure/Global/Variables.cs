﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Infrastructure.Global
{
    public static class Variables
    {
        public static DateTime TimeLastTrafficJamUpdate { get; set; }
    }
}
