﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApplication1.Infrastructure.Models;

namespace WebApplication1.Infrastructure.Repositories.RoadRepository
{
    public class RoadRepository : IRoadRepository
    {
        private SqlDbContext context;

        public RoadRepository(SqlDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Region> GetAllAvailableRegions()
        {
            return context.Regions;
        }

        public void SaveAllAvailableRegions(IEnumerable<Region> regions)
        {
            if (context.Regions != null && context.Regions.Any())
            {
                context.Regions.RemoveRange(context.Regions);
            }
            context.Regions.AddRange(regions);

            context.SaveChanges();
        }

        public void SaveAllTrafficJamState(IEnumerable<RegionTrafficJamState> trafficJams)
        {
            if (context.RegionTrafficJamStates != null && context.RegionTrafficJamStates.Any())
            {
                context.RegionTrafficJamStates.RemoveRange(context.RegionTrafficJamStates);
            }
            context.RegionTrafficJamStates.AddRange(trafficJams);

            context.SaveChanges();
        }

        public void SaveTrafficJamState(RegionTrafficJamState trafficJam)
        {
            if (context.RegionTrafficJamStates.Contains(trafficJam))
            {
                context.RegionTrafficJamStates.Update(trafficJam);
            }
            else
            {
                context.RegionTrafficJamStates.Add(trafficJam);
            }

            context.SaveChanges();
        }

        public IEnumerable<RegionTrafficJamState> GetAllTrafficJam()
        {
            return context.RegionTrafficJamStates;
        }

        public bool IsRegionExist(string region)
        {
            return context.Regions.Any(r=>string.CompareOrdinal(r.RegionCode, region) == 0 );
        }

        public RegionTrafficJamState GetTrafficJamByRegion(string region)
        {
            return context.RegionTrafficJamStates.FirstOrDefault(r=> string.CompareOrdinal(r.Region, region) == 0);
        }

        
    }
}
