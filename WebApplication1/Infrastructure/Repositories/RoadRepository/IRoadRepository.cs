﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Infrastructure.Models;

namespace WebApplication1.Infrastructure.Repositories.RoadRepository
{
    public interface IRoadRepository
    {
        IEnumerable<Region> GetAllAvailableRegions();

        void SaveAllAvailableRegions(IEnumerable<Region> regions);

        void SaveAllTrafficJamState(IEnumerable<RegionTrafficJamState> regions);

        void SaveTrafficJamState(RegionTrafficJamState trafficJam);

        IEnumerable<RegionTrafficJamState> GetAllTrafficJam();

        bool IsRegionExist(string region);

        RegionTrafficJamState GetTrafficJamByRegion(string region);
    }
}
