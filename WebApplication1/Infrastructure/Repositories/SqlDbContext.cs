﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Infrastructure.Models;

namespace WebApplication1.Infrastructure.Repositories
{
    public class SqlDbContext : DbContext
    {
        public DbSet<Region> Regions { get; set; }

        public DbSet<RegionTrafficJamState> RegionTrafficJamStates { get; set; }

        public SqlDbContext(DbContextOptions<SqlDbContext> options): base(options)
        {}
    }
}
